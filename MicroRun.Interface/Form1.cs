﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Gma.UserActivityMonitor;
using MicroRun.Brain;
using System.Linq;

namespace Storm.Interface
{
    public partial class Form1 : Form
    {
        //private Neuron[] _input = new Neuron[1000];
        //private Neuron[] _output = new Neuron[1000];
        private List<Neuron> _huppoCampas = new List<Neuron>();
        private List<Neuron> _neuron = new List<Neuron>();
        private Neuron _prevNeuron;
        private string _strTracking = "";
        private int _found = 1;
        private int _group = 1;
        private int _runningCount = 0;

        public List<Neuron> Neuron
        {
            get { return _neuron; }
            set { _neuron = value; }
        }

        //public Neuron[] Output
        //{
        //    get { return _output; }
        //    set { _output = value; }
        //}

        public List<Neuron> HuppoCampas
        {
            get { return _huppoCampas; }
            set { _huppoCampas = value; }
        }

        public List<Neuron> ActiveNeurons { get; set; }

        public string Tracking
        {
            get { return _strTracking; }
            set { _strTracking = value; }
        }

        public int Group
        {
            get { return _group; }
            set { _group = value; }
        }

        public int RunningCount
        {
            get { return _runningCount; }
            set { _runningCount = value; }
        }

        public Form1()
        {
            InitializeComponent();
            Neuron neuron = new Neuron(0, "Storm.HuppoCampas");

            this.ActiveNeurons = new List<Neuron>();
            RunningCount = 0;
            //HookManager.KeyPress += HookManager_KeyPress;
        }

        private void HookManager_KeyPress(object sender, KeyPressEventArgs e)
        {


        }

        private void btnSleep_Click(object sender, EventArgs e)
        {
            //Neuron.AddNeuron("test", -1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string dicPath = "D:\\DevTools\\SpellCheckerExample\\Dicts\\Standard\\American.txt";
            StreamReader sr = new StreamReader(dicPath);

            while (!sr.EndOfStream)
            {
                Group++;
                // Neuron.AddNeuron(sr.ReadLine(), Group);
            }

        }

        private void btnAnalyse_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Stopwatch st = new System.Diagnostics.Stopwatch();
            st.Start();
            // Neuron.FindNeuron(null, -1);
            st.Stop();
            txtSystem.Text = st.Elapsed.Milliseconds.ToString() + " milliseconds";
        }

        private void txtSystem_KeyDown(object sender, KeyEventArgs e)
        {
            //Tracking += e.KeyData;

            //if (Tracking.Contains("\r"))
            //{
            //    Input[RunningCount] = new Neuron(RunningCount, e.KeyData.ToString());

            //    Group++;
            //    Tracking = "";
            //    ActiveNeurons.Clear();
            //}
        }

        private void btnHuman_Click(object sender, EventArgs e)
        {
            var existingNeuron = Neuron.FirstOrDefault(x => x != null && x.Value == txtSystem.Text);
            if (existingNeuron == null)
            {
                _prevNeuron = new Neuron(RunningCount, txtSystem.Text);
                Neuron.Add(_prevNeuron);
                RunningCount++;

            }
            else
            {
                if (existingNeuron.Value != _prevNeuron.Value)
                {
                    existingNeuron.Children.Add(_prevNeuron);
                    _prevNeuron.Parents.Add(existingNeuron);
                    HuppoCampas.Add(existingNeuron);
                }
                existingNeuron.Hit++;
            }
        }

        private void btnComputer_Click(object sender, EventArgs e)
        {
            Neuron bestNeuron = null;
            foreach (var neuron in Neuron.OrderByDescending(x => x.Hit))
            {
                if (neuron.Value == txtSystem.Text)
                {
                    bestNeuron = neuron;
                    break;
                }
                else
                {
                    bestNeuron = neuron.FindNeuron(txtSystem.Text, 0);
                    if (bestNeuron != null)
                        break;
                }
            }
            if (bestNeuron != null)
            {
                var parentNeuron = bestNeuron.Parents.OrderByDescending(x => x.Hit).FirstOrDefault();
                if (parentNeuron != null)
                {
                    txtBrain.Text = parentNeuron.Value;
                }
                else
                {
                    var childNeuron = bestNeuron.Children.OrderByDescending(x => x.Hit).FirstOrDefault();
                    if (childNeuron != null)
                    {
                        if (childNeuron.Hit > bestNeuron.Hit)
                        {
                            txtBrain.Text = childNeuron.Value;
                        }
                        else
                        {
                            txtBrain.Text = bestNeuron.Value;
                        }
                    }
                    else
                    {
                        txtBrain.Text = "NothingC";
                    }
                }
            }
            else
            {
                txtBrain.Text = "NothingP";
            }
        }
    }
}
