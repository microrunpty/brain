﻿namespace Storm.Interface
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSleep = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAnalyse = new System.Windows.Forms.Button();
            this.txtSystem = new System.Windows.Forms.TextBox();
            this.txtBrain = new System.Windows.Forms.TextBox();
            this.btnHuman = new System.Windows.Forms.Button();
            this.btnComputer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSleep
            // 
            this.btnSleep.Location = new System.Drawing.Point(100, 70);
            this.btnSleep.Name = "btnSleep";
            this.btnSleep.Size = new System.Drawing.Size(75, 23);
            this.btnSleep.TabIndex = 0;
            this.btnSleep.Text = "Sleep";
            this.btnSleep.UseVisualStyleBackColor = true;
            this.btnSleep.Click += new System.EventHandler(this.btnSleep_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(100, 116);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Read Dic";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAnalyse
            // 
            this.btnAnalyse.Location = new System.Drawing.Point(100, 155);
            this.btnAnalyse.Name = "btnAnalyse";
            this.btnAnalyse.Size = new System.Drawing.Size(75, 23);
            this.btnAnalyse.TabIndex = 2;
            this.btnAnalyse.Text = "Analyse";
            this.btnAnalyse.UseVisualStyleBackColor = true;
            this.btnAnalyse.Click += new System.EventHandler(this.btnAnalyse_Click);
            // 
            // txtSystem
            // 
            this.txtSystem.Location = new System.Drawing.Point(221, 65);
            this.txtSystem.Multiline = true;
            this.txtSystem.Name = "txtSystem";
            this.txtSystem.Size = new System.Drawing.Size(242, 113);
            this.txtSystem.TabIndex = 3;
            this.txtSystem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSystem_KeyDown);
            // 
            // txtBrain
            // 
            this.txtBrain.Location = new System.Drawing.Point(513, 65);
            this.txtBrain.Multiline = true;
            this.txtBrain.Name = "txtBrain";
            this.txtBrain.Size = new System.Drawing.Size(242, 113);
            this.txtBrain.TabIndex = 4;
            // 
            // btnHuman
            // 
            this.btnHuman.Location = new System.Drawing.Point(280, 199);
            this.btnHuman.Name = "btnHuman";
            this.btnHuman.Size = new System.Drawing.Size(103, 23);
            this.btnHuman.TabIndex = 5;
            this.btnHuman.Text = "Human Speak";
            this.btnHuman.UseVisualStyleBackColor = true;
            this.btnHuman.Click += new System.EventHandler(this.btnHuman_Click);
            // 
            // btnComputer
            // 
            this.btnComputer.Location = new System.Drawing.Point(582, 199);
            this.btnComputer.Name = "btnComputer";
            this.btnComputer.Size = new System.Drawing.Size(75, 23);
            this.btnComputer.TabIndex = 6;
            this.btnComputer.Text = "Computer Speak";
            this.btnComputer.UseVisualStyleBackColor = true;
            this.btnComputer.Click += new System.EventHandler(this.btnComputer_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 273);
            this.Controls.Add(this.btnComputer);
            this.Controls.Add(this.btnHuman);
            this.Controls.Add(this.txtBrain);
            this.Controls.Add(this.txtSystem);
            this.Controls.Add(this.btnAnalyse);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSleep);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSleep;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnAnalyse;
        private System.Windows.Forms.TextBox txtSystem;
        private System.Windows.Forms.TextBox txtBrain;
        private System.Windows.Forms.Button btnHuman;
        private System.Windows.Forms.Button btnComputer;
    }
}

