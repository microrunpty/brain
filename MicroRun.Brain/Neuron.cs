﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MicroRun.Brain
{
    public class Neuron
    {
        private int _id = 0;
        private int _group = 0;
        private string _value = "";
        private List<Dendrit> _dendrites = new List<Dendrit>();
        private List<Axon> _axons = new List<Axon>();

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public int Group
        {
            get
            {
                return _group;
            }
            set
            {
                _group = value;
            }
        }

        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        public List<Dendrit> Dendrites
        {
            get
            {
                return _dendrites;
            }
            set
            {
                _dendrites = value;
            }
        }

        public List<Axon> Axons
        {
            get
            {
                return _axons;
            }
            set
            {
                _axons = value;
            }
        }

        public List<Neuron> Parents { get; set; }

        public List<Neuron> Children { get; set; }

        public int Hit { get; set; }

        public Neuron(int id, string value)
        {
            Id = id;
            Value = value;
            Children = new List<Neuron>();
            Parents = new List<Neuron>();
        }

        public Neuron(int id, string value, int group, Neuron neuron)
        {
            Id = id;
            Value = value;
            Group = group;
            Parents = new List<Neuron>();
            Children = new List<Neuron>();
            Parents.Add(neuron);
        }

        public Neuron AddNeuron(string value, int group)
        {
            var neuron = this.FindNeuron(value, group);
            if (neuron == null)
            {
                neuron = new Neuron(this.Id, value, group, this);
                Children.Add(neuron);

                this.Id = Id + 1;
            }

            return neuron;
        }

        public void PushNeuron(string value, int group, List<Neuron> neurons)
        {
            var neuron = this.FindNeuron(value, group);
            if (neuron == null)
            {
                neuron = new Neuron(this.Id, value, group, this);
                Children.Add(neuron);

                this.Id = Id + 1;
            }

            foreach (var n in neurons)
            {
                if (n != neuron)
                {
                    this.Children.Remove(n);
                }
                n.Parents.Add(neuron);
            }

            neuron.Children.AddRange(neurons.Where(n => n != neuron));   
        }

        public Neuron FindNeuron(string value, int group)
        {
            foreach (var neuron in this.Children)
            {
                var trueCheck = neuron.Value == value;
                if (trueCheck)
                {
                    neuron.Hit++;
                    return neuron;
                }

                var foundNeuron = neuron.FindNeuron(value, group);

                if (foundNeuron != null)
                {
                    return foundNeuron;
                }
            }

            return null;
        }
    }
}
