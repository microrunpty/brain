﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroRun.Brain
{
    public class Axon
    {
        private Dendrit _dendrit = new Dendrit();

        public Dendrit Dendrit
        {
            get { return _dendrit; }
            set {_dendrit = value;}
        }
    }
}
