﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroRun.Brain
{
    public class Dendrit
    {
        private Neuron _neuron;

        public Neuron Neuron
        {
            get { return _neuron; }
            set { _neuron = value; }
        }
    }
}
